require 'types/mutation_type'
BeautyLandBackendSchema = GraphQL::Schema.define do
  mutation(Types::MutationType)
  query(Types::QueryType)

 use GraphQL::Batch

 max_depth 12
 max_complexity 1000

 resolve_type lambda { |_obj, _ctx|}
end

BeautyLandBackendSchema.middleware << GraphQL::Schema::TimeoutMiddleware.new(max_seconds: 10) do |err, query|
  Rails.logger.info("GraphQL Timeout: #{query.query_string}")
end

log_query_complexity = GraphQL::Analysis::QueryComplexity.new { |query, complexity| Rails.logger.info("[GraphQL Query Complexity] #{complexity}")}
BeautyLandBackendSchema.query_analyzers << log_query_complexity

BeautyLandBackendSchema.query_analyzers << GraphQL::Analysis::FieldUsage.new { |query, used_fields, used_deprecated_fields|
  puts "Used GraphQL fields: #{used_fields.join(', ')}"
  puts "Used deprecated GraphQL fields: #{used_deprecated_fields.join(', ')}"
}
