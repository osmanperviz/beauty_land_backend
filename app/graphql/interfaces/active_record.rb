Interfaces::ActiveRecord = GraphQL::InterfaceType.define do
  name 'ActiveRecordInterface'
  description "Active Record Interface"


  field :id, types.Int, 'The ID of the entity'

  field :updated_at do
    type types.Int
    description 'The updated_at of the entity'
    resolve -> (obj, args, ctx) {
      obj.updated_at.to_i
    }
  end

  field :created_at do
    type types.Int
    description 'The created_at of the entity'

    resolve -> (obj, args, ctx) {
      obj.created_at.to_i
    }
  end
end
