Interfaces::UserModel = GraphQL::InterfaceType.define do
  name 'UserInterface'
  description "User Interface"

  field :name, types.String, 'The NAME of the user'
  field :email, types.String, 'The EMAIL of the user'
  field :api_token, types.String, 'The API TOKEN of the user'
  field :username, types.String, 'The USERNAME of the user'
  field :type, types.String, 'The TYPE of the user'
end
