Types::CompanyType = GraphQL::ObjectType.define do
  interfaces [Interfaces::ActiveRecord]
  name 'CompanyType'
  description 'Represents a company model'


  field :name, types.String, 'The NAME of the company'

  field :address, types.String, 'The ADDRESS of the company'

  field :email, types.String, 'The EMAIL of the company'

  field :owner, Types::OwnerType, 'The OWNER of the company' do
    resolve -> (company, args, ctx) { company.owner }
  end

  field :stores, !types[Types::StoreType], 'Company associated STORES' do
    resolve -> (company, args, ctx) { company.stores }
  end

end
