Types::EmployeeType = GraphQL::ObjectType.define do
  interfaces [Interfaces::ActiveRecord, Interfaces::UserModel]
  name 'EmployeeType'
  description 'Represents a employee model'

  field :store, Types::StoreType, 'Related stores' do
    resolve -> (employee, args, ctx) { employee.store }
  end
end
