Types::OwnerType = GraphQL::ObjectType.define do
  interfaces [Interfaces::ActiveRecord, Interfaces::UserModel]
  name 'OwnerType'
  description 'Represents a owner model'

  field :company, Types::CompanyType, 'Related Company' do
    resolve -> (owner, args, ctx) { owner.company }
  end
end
