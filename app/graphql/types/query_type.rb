Types::QueryType = GraphQL::ObjectType.define do
  name "Query"

  field :companies, !types[Types::CompanyType], "Company List" do
   resolve ->(obj, args, ctx) { Company.all }
  end

  field :company, Types::CompanyType, "Single Company" do
   argument :id, types.ID
   resolve ->(obj, args, ctx) { Company.find(args.id) }
  end

  field :stores, !types[Types::StoreType], "Stores List" do
   resolve ->(obj, args, ctx) {
      Store.all
   }
  end

  field :store, Types::StoreType, "Stores List" do
   argument :id, types.ID
   resolve ->(obj, args, ctx) { Store.find(args.id) }
  end

  field :employee, Types::StoreType, "Stores List" do
   resolve ->(obj, args, ctx) { Store.all }
  end
end
