Types::StoreServiceType = GraphQL::ObjectType.define do
  interfaces [Interfaces::ActiveRecord]
  name 'StoreServiceType'
  description 'Store specific service'

  field :name, types.String, 'The NAME of the service'
  field :description, types.String, 'The DESCRIPTION of the service'
  field :store, Types::StoreType, 'Related store' do
    resolve -> (service, args, ctx) { service.store }
  end
end
