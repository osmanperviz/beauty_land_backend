Types::StoreType = GraphQL::ObjectType.define do
  interfaces [Interfaces::ActiveRecord, Interfaces::UserModel]
  name 'StoreType'
  description 'Represents a Store model'

  field :company, Types::CompanyType, 'Related Company' do
    resolve -> (store, args, ctx) { store.company }
  end

  field :employees, !types[Types::EmployeeType], 'Store employees' do
    resolve -> (store, args, ctx) { store.employees }
  end

  field :store_services, !types[Types::StoreServiceType], 'Store services' do
    resolve -> (store, args, ctx) { store.store_services }
  end

end
