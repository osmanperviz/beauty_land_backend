class Company < ApplicationRecord
  belongs_to :owner,
             class_name: 'User',
             foreign_key: 'user_id'

  has_many :stores

  validates_presence_of :name, :address, :email, :user_id
  validates :email, uniqueness: true

end
