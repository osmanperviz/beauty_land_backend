class Employee < User
  belongs_to :store,
             foreign_key: 'store_id'

  validates :store, presence: true
end
