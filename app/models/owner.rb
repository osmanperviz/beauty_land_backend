class Owner < User
  has_one :company,
          foreign_key: 'user_id'

  validates_presence_of :company        

end
