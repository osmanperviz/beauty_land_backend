class Store < ApplicationRecord
  belongs_to :company

  has_many :employees,
           class_name: 'User',
           foreign_key: 'store_id'

  has_many :store_services,
            foreign_key: 'store_id'

  validates_presence_of :name, :email, :street, :city
  validates :email, uniqueness: true
end
