class AuthorizeApiRequest

  def initialize(headers = {})
    @headers = headers
  end

  def perform!
    current_user
  end

  private

  attr_reader :headers

  def current_user
    @current_user ||= User.find(decoded_auth_token[:user_id]) if decoded_auth_token
    @current_user || GraphQL::ExecutionError.new("Invalid Token!")
  end

  def decoded_auth_token
   @decoded_auth_token ||= JsonWebTokenService.decode(http_auth_header)
 end

 def http_auth_header
   return nil unless authorization_header_present?
   extracted_token
 end

 def authorization_header_present?
   headers['Authorization'].present?
 end

 def extracted_token
   headers['Authorization'].split(' ').last
 end
end
