class JsonWebTokenService
  # ENCRYPTION_KEY = Rails.application.secrets.secret_key_base
  ENCRYPTION_KEY = 'test'

  class << self
    def encode(payload, exp = 2.hours.from_now)
      payload[:exp] = exp.to_i

      JWT.encode(payload, ENCRYPTION_KEY)
    end

    def decode(token)
      body = JWT.decode(token, ENCRYPTION_KEY)[0]
      HashWithIndifferentAccess.new body

    # raise custom error to be handled by custom handler
    rescue JWT::ExpiredSignature, JWT::VerificationError => e
      raise GraphQL::ExecutionError.new e.message
    rescue JWT::DecodeError, JWT::VerificationError => e
      raise GraphQL::ExecutionError.new e.message
    end
  end
end
