class AddEmailColumnToCompany < ActiveRecord::Migration[5.1]
  def change
    add_column(:companies, :email, :string, null: false)
  end
end
