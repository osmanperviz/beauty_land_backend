class CreateStores < ActiveRecord::Migration[5.1]
  def change
    create_table :stores do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :email
      t.string :company_id, null: false

      t.timestamps
    end
  end
end
