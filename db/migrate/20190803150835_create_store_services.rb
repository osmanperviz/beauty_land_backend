class CreateStoreServices < ActiveRecord::Migration[5.1]
  def change
    create_table :store_services do |t|
      t.string :name
      t.text :description
      t.string :store_id, null: false
      t.timestamps
    end
  end
end
