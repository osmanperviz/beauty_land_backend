FactoryBot.define do
  factory :company do
    name Faker::Company.name
    address Faker::Address.street_address
    email Faker::Internet.email
    after(:build) do |company|
      company.owner = create(:owner, company: company)
    end
  end
end
