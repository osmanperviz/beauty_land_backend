FactoryBot.define do
  factory :employee do
    name Faker::Name.name
    email Faker::Internet.email
    api_token Faker::Crypto.md5
    username Faker::Name.name
    password_digest "Some pass"
    association :store, factory: :store
  end
end
