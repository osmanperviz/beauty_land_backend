FactoryBot.define do
  factory :owner do
    name Faker::Name.name
    email Faker::Internet.email
    api_token Faker::Crypto.md5
    username Faker::Name.name
    password_digest "Some pass"
    association :company, factory: :company
  end
end
