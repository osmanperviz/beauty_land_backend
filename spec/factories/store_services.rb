FactoryBot.define do
  factory :store_service do
    name Faker::Food.dish
    description Faker::Food.description
    association :store, factory: :store
  end
end
