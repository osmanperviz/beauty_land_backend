FactoryBot.define do
  factory :store do
    name Faker::Company.name
    city Faker::Address.city
    street Faker::Address.street_address
    email Faker::Internet.email
    association :company, factory: :company
  end
end
