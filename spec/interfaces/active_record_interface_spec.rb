# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Interfaces::ActiveRecord do

  it { is_expected.to have_field(:id).of_type("Int") }
  it { is_expected.to have_field(:updated_at).of_type("Int") }
  it { is_expected.to have_field(:created_at).of_type("Int") }

end
