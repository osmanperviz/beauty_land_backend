# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Interfaces::UserModel do

  it { is_expected.to have_field(:name).of_type("String") }
  it { is_expected.to have_field(:email).of_type("String") }
  it { is_expected.to have_field(:api_token).of_type("String") }
  it { is_expected.to have_field(:username).of_type("String") }
  it { is_expected.to have_field(:type).of_type("String") }

end
