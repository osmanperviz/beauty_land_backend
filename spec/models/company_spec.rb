require 'rails_helper'

describe Company, type: :model do
  it { is_expected.to have_attribute(:email) }
  it { is_expected.to have_attribute(:name) }
  it { is_expected.to have_attribute(:address) }
  it { is_expected.to have_attribute(:user_id) }

  it { should belong_to(:owner).class_name('User') }
  it { should have_many(:stores) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:address) }
  it { should validate_presence_of(:user_id) }

  it "validates uniqueness of name" do
    create(:company)

    should validate_uniqueness_of(:email)
  end
end
