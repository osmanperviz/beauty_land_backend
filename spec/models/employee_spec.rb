require 'rails_helper'

RSpec.describe Employee, type: :model do
  it { should  belong_to(:store) }
  
  it { is_expected.to validate_presence_of(:store) }
end
