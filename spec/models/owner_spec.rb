require 'rails_helper'

RSpec.describe Owner, type: :model do
  it { should  have_one(:company) }
  it { is_expected.to validate_presence_of(:company) }
end
