require 'rails_helper'

RSpec.describe StoreService, type: :model do
  it { should  belong_to(:store) }
  it { is_expected.to have_attribute(:name) }
  it { is_expected.to have_attribute(:description) }


  it { is_expected.to validate_presence_of(:store) }
end
