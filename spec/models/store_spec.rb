require 'rails_helper'

RSpec.describe Store, type: :model do
  it { is_expected.to have_attribute(:email) }
  it { is_expected.to have_attribute(:name) }
  it { is_expected.to have_attribute(:street) }
  it { is_expected.to have_attribute(:city) }

  it { should belong_to(:company) }
  it { should have_many(:employees).class_name('User') }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:street) }
  it { should validate_presence_of(:city) }


end
