require 'rails_helper'

RSpec.describe User, type: :model do


  it { is_expected.to have_attribute(:type) }
  it { is_expected.to have_attribute(:name) }
  it { is_expected.to have_attribute(:username) }
  it { is_expected.to have_attribute(:store_id) }


  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:username) }
  it { should have_secure_password }

  it "validates uniqueness of name" do
    create(:user, email: 'test@test.com', type: "Owner")

    should validate_uniqueness_of(:email)
  end
end
