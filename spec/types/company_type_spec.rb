# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Types::CompanyType do
  it { is_expected.to implement(Interfaces::ActiveRecord) }

  it { is_expected.to have_field(:name).of_type("String") }
  it { is_expected.to have_field(:address).of_type("String") }
  it { is_expected.to have_field(:email).of_type("String") }
  it { is_expected.to have_field(:name).of_type("String") }

  describe 'owner' do
    subject { described_class.fields['owner'] }
    it { is_expected.to be_of_type('OwnerType') }
  end
  describe 'stores' do
    subject { described_class.fields['stores'] }
    it { is_expected.to be_of_type('[StoreType]') }
  end
end
