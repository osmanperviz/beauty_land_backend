# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Types::EmployeeType do
  it { is_expected.to implement(Interfaces::ActiveRecord) }
  it { is_expected.to implement(Interfaces::UserModel) }

  describe 'store' do
    subject { described_class.fields['store'] }
    it { is_expected.to be_of_type('StoreType') }
  end
end
