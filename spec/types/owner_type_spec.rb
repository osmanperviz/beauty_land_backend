# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Types::OwnerType do
  it { is_expected.to implement(Interfaces::ActiveRecord) }
  it { is_expected.to implement(Interfaces::UserModel) }

  describe 'company' do
    subject { described_class.fields['company'] }
    it { is_expected.to be_of_type('CompanyType') }
  end
end
