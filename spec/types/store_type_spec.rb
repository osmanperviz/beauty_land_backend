# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Types::StoreType do
  it { is_expected.to implement(Interfaces::ActiveRecord) }
  it { is_expected.to implement(Interfaces::UserModel) }

  describe 'company' do
    subject { described_class.fields['company'] }
    it { is_expected.to be_of_type('CompanyType') }
  end
  describe 'employees' do
    subject { described_class.fields['employees'] }
    it { is_expected.to be_of_type('[EmployeeType]!') }
  end
end
